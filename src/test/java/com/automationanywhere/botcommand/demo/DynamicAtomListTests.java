package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import Utils.DynamicAtomSelectLogic;
import Utils.DynamicProcessSelectLogic;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.SelectItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamicAtomListTests {

    public static void main(String[] args){

        String AccountID = "automationanywhere-JJF9AK";
        String Username = "brendan.sapience@automationanywhere.com";
        String Password = "ORTORIBLeRigripULAGANtRATeRopHyadaYmANadacKHaeNtiLanCiTerAzA";
        String SessionName = "Default";

        DynamicAtomSelectLogic command = new DynamicAtomSelectLogic();

        BoomiSession myBackendServ = new BoomiSession(BoomiUtils.StringToSecureString(AccountID),BoomiUtils.StringToSecureString(Username),BoomiUtils.StringToSecureString(Password),false);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        Map<String, Value> inputMap = new HashMap<String,Value>();
        inputMap.put("sessionName",new StringValue(SessionName));

        List<SelectItem> d = command.execute(inputMap);

        int i=0;
        for(SelectItem item : d){
            System.out.println("Row ["+i+"]: "+item.getLabel()+" | "+item.getValue());
            i++;
        }



    }
}
