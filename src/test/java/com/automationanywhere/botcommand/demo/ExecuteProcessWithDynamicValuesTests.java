package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import Utils.DynamicAtomSelectLogic;
import Utils.DynamicProcessSelectLogic;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.SelectItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteProcessWithDynamicValuesTests {

    public static void main(String[] args){

        String AccountID = "automationanywhere-JJF9AK";
        String Username = "brendan.sapience@automationanywhere.com";
        String Password = "ORTORIBLeRigripULAGANtRATeRopHyadaYmANadacKHaeNtiLanCiTerAzA";
        String SessionName = "Default";
        String ProcessProperties = "\"ProcessProperties\" : {\"@type\" : \"ProcessProperties\",\"ProcessProperty\" : [{\"@type\" : \"\",\"Name\" : \"priority\",\"Value\": \"medium\"}]}";

        ExecuteProcess command = new ExecuteProcess();

        DynamicProcessSelectLogic DynSelectProc = new DynamicProcessSelectLogic();
        DynamicAtomSelectLogic DynSelectAtom = new DynamicAtomSelectLogic();

        BoomiSession myBackendServ = new BoomiSession(BoomiUtils.StringToSecureString(AccountID),BoomiUtils.StringToSecureString(Username),BoomiUtils.StringToSecureString(Password),false);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);
        DynSelectProc.setSessions(mso);
        DynSelectAtom.setSessions(mso);

        Map<String, Value> inputMap = new HashMap<String,Value>();
        inputMap.put("sessionName",new StringValue(SessionName));

        List<SelectItem> AllProcesses = DynSelectProc.execute(inputMap);
        List<SelectItem> AllAtoms = DynSelectAtom.execute(inputMap);

        String BrenAtom = AllAtoms.get(0).getValue();
        String StdProcess = AllProcesses.get(1).getValue();

        System.out.println("DEBUG: Atom:"+BrenAtom);
        System.out.println("DEBUT: Process:"+StdProcess);

        NumberValue d = command.action(SessionName,StdProcess,BrenAtom,ProcessProperties);

        System.out.println("DEBUG Ret Code: "+d.get().intValue());



    }
}
