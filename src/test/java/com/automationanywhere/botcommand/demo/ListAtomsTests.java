package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.HashMap;
import java.util.Map;

public class ListAtomsTests {

    public static void main(String[] args){

        String AccountID = "automationanywhere-JJF9AK";
        String Username = "brendan.sapience@automationanywhere.com";
        String Password = "ORTORIBLeRigripULAGANtRATeRopHyadaYmANadacKHaeNtiLanCiTerAzA";
        String SessionName = "Default";

        ListAtoms command = new ListAtoms();
//{"@type":"QueryResult","result":[{"@type":"Atom","id":"066c493e-4680-4651-9796-2b0411e77322","name":"BrenLocalVM1","status":"ONLINE","type":"ATOM","hostName":"192.168.0.47","dateInstalled":"2020-09-03T16:44:56Z","currentVersion":"20.09.0","purgeHistoryDays":30,"purgeImmediate":false,"forceRestartTime":0},{"@type":"Atom","id":"4f1b1297-88e6-4a7c-b6e1-7da4e06c1a05","name":"JuliaLocalVM1","status":"ONLINE","type":"ATOM","hostName":"192.168.0.43","dateInstalled":"2020-09-04T17:38:17Z","currentVersion":"20.09.0","purgeHistoryDays":30,"purgeImmediate":false,"forceRestartTime":0},{"@type":"Atom","id":"560a2c07-482f-40c6-ad9e-1d7ea749bdeb","name":"Test Atom Cloud","status":"ONLINE","type":"CLOUD","hostName":"USA West Integration Test Cloud 01","dateInstalled":"2020-08-31T15:51:10Z","purgeHistoryDays":0,"purgeImmediate":false,"forceRestartTime":60000,"cloudId":"6550b6ba-d0a8-4b6e-9a53-e108bb943371","instanceId":"automationanywhere-JJF9AK.BNB07S"}],"numberOfResults":3}
        BoomiSession myBackendServ = new BoomiSession(BoomiUtils.StringToSecureString(AccountID),BoomiUtils.StringToSecureString(Username),BoomiUtils.StringToSecureString(Password),false);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        StringValue d = command.action(SessionName);

        System.out.println(d.get());
        /*
        Table table = d.get();
        List<Row> AllRows = table.getRows();

        for(int i=0;i<AllRows.size();i++){

            Row dv = AllRows.get(i);
            String aRowOfStrings = "";
            for(int j = 0;j < dv.getValues().size();j++){
                Value myVal = dv.getValues().get(j);
                if(aRowOfStrings.equals("")){
                    aRowOfStrings = myVal.get().toString();
                }else{
                    aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                }

            }
            System.out.println("DEBUG Row "+i+": "+aRowOfStrings);

        }
*/
    }
}
