package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteProcessTests {

    public static void main(String[] args){

        String AccountID = "automationanywhere-JJF9AK";
        String Username = "brendan.sapience@automationanywhere.com";
        String Password = "ORTORIBLeRigripULAGANtRATeRopHyadaYmANadacKHaeNtiLanCiTerAzA";
        String SessionName = "Default";
        String ProcessID = "96643805-e552-47ff-ada3-e9147182d1d4";
        String AtomID = "066c493e-4680-4651-9796-2b0411e77322";
        String ProcessProperties = "\"ProcessProperties\" : {\"@type\" : \"ProcessProperties\",\"ProcessProperty\" : [{\"@type\" : \"\",\"Name\" : \"priority\",\"Value\": \"medium\"}]}";

        ExecuteProcess command = new ExecuteProcess();

        BoomiSession myBackendServ = new BoomiSession(BoomiUtils.StringToSecureString(AccountID),BoomiUtils.StringToSecureString(Username),BoomiUtils.StringToSecureString(Password),false);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        NumberValue d = command.action(SessionName,ProcessID,AtomID,ProcessProperties);

        System.out.println("DEBUG Ret Code: "+d.get().intValue());



    }
}
