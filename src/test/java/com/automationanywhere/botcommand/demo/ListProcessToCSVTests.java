package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListProcessToCSVTests {

    public static void main(String[] args){

        String AccountID = "automationanywhere-JJF9AK";
        String Username = "brendan.sapience@automationanywhere.com";
        String Password = "ORTORIBLeRigripULAGANtRATeRopHyadaYmANadacKHaeNtiLanCiTerAzA";
        String SessionName = "Default";
        String JSONList = "{\"@type\":\"QueryResult\",\"result\":[{\"@type\":\"Process\",\"IntegrationPack\":[],\"name\":\"New Process\",\"id\":\"ab223099-bf9b-4985-a8c5-5cad23d06929\"},{\"@type\":\"Process\",\"IntegrationPack\":[],\"name\":\"Customer Info - CSV To DB\",\"id\":\"96643805-e552-47ff-ada3-e9147182d1d4\"}],\"numberOfResults\":2}";

        ListProcessToTable command = new ListProcessToTable();

        BoomiSession myBackendServ = new BoomiSession(BoomiUtils.StringToSecureString(AccountID),BoomiUtils.StringToSecureString(Username),BoomiUtils.StringToSecureString(Password),false);

        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        TableValue d = command.action(JSONList);
        Table table = d.get();
        List<Row> AllRows = table.getRows();

        for(int i=0;i<AllRows.size();i++){

            Row dv = AllRows.get(i);
            String aRowOfStrings = "";
            for(int j = 0;j < dv.getValues().size();j++){
                Value myVal = dv.getValues().get(j);
                if(aRowOfStrings.equals("")){
                    aRowOfStrings = myVal.get().toString();
                }else{
                    aRowOfStrings = aRowOfStrings +","+myVal.get().toString();
                }

            }
            System.out.println("DEBUG Row "+i+": "+aRowOfStrings);

        }

    }
}
