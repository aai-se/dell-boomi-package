/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.SelectItem;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.DesktopOperationsSelectExecute;
import com.automationanywhere.commandsdk.annotations.Sessions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@BotCommand(commandType = BotCommand.CommandType.DesktopOperationSelect)
@CommandPkg(name = "DynamicAtomSelectLogic")
public class DynamicAtomSelectLogic {
    private static final Logger logger = LogManager.getLogger(DynamicAtomSelectLogic.class);

    //@Sessions
    private Map<String, Object> sessions;

    @DesktopOperationsSelectExecute
    public List<SelectItem> execute(Map<String, Value> input) {

        // List of Items to populate the Dynamic Dropdown
        List<SelectItem> retVal = new ArrayList<>();

       String SessionName = (String) input.get("sessionName").get();

        BoomiSession serv = (BoomiSession) this.sessions.get(SessionName);
        RestRequests BoomiRequests = new RestRequests(serv);
        String JsonResp = BoomiRequests.GetListOfAtoms();
        try {
            retVal = RestResponse.ProcessAtomListResponseAsListOfItems(JsonResp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    public void setSessions(Map<String, Object> sessions) { this.sessions = sessions; }
}

