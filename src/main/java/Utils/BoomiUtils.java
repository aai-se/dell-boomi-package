package Utils;

import com.automationanywhere.core.security.SecureString;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class BoomiUtils {

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static CredentialsProvider GetCredsProvider(BoomiSession session){

        String name = session.getSecureLogin().getInsecureString();
        String password = session.getSecurePwd().getInsecureString();
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(name, password);
        provider.setCredentials(AuthScope.ANY, credentials);

        return provider;
    }

    public static SecureString StringToSecureString(String myString){

        char[] SecStringArr = new char[myString.length()];
        for (int i = 0; i < myString.length(); i++) { SecStringArr[i] = myString.charAt(i); }
        SecureString secString = new SecureString(SecStringArr);
        return secString;
    }

}
