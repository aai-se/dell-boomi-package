package Utils;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;

public class RestRequests {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    private BoomiSession bSession;

    private static String BOOMI_BAS_URL = "https://api.boomi.com/api/rest/v1";
    public RestRequests(BoomiSession session){this.bSession = session; }

    public String GetListOfExecutionRecords() {

        CredentialsProvider provider = BoomiUtils.GetCredsProvider(this.bSession);
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

        // URL
        String CompleteURL = BOOMI_BAS_URL + "/"+this.bSession.getAccountID().getInsecureString()+"/executionrecord/query";

        try {
            HttpPost request = new HttpPost(CompleteURL);
            request.addHeader("Accept", "application/json");
            CloseableHttpResponse response = client.execute(request);

            HttpEntity entity = response.getEntity();

            String JsonResponse = EntityUtils.toString(response.getEntity());
            return JsonResponse;

        } catch (ClientProtocolException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        } catch (IOException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
    }


    public String GetListOfAtoms() {

        CredentialsProvider provider = BoomiUtils.GetCredsProvider(this.bSession);
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

        // URL
        String CompleteURL = BOOMI_BAS_URL + "/"+this.bSession.getAccountID().getInsecureString()+"/Atom/query";

        try {
            HttpPost request = new HttpPost(CompleteURL);
            request.addHeader("Accept", "application/json");
            CloseableHttpResponse response = client.execute(request);

            HttpEntity entity = response.getEntity();

            String JsonResponse = EntityUtils.toString(response.getEntity());
            return JsonResponse;

        } catch (ClientProtocolException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        } catch (IOException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
    }

    public String GetListOfProcesses() {

        CredentialsProvider provider = BoomiUtils.GetCredsProvider(this.bSession);
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

        // URL
        String CompleteURL = BOOMI_BAS_URL + "/"+this.bSession.getAccountID().getInsecureString()+"/Process/query";

        try {
            HttpPost request = new HttpPost(CompleteURL);
            request.addHeader("Accept", "application/json");
            CloseableHttpResponse response = client.execute(request);

            HttpEntity entity = response.getEntity();

            String JsonResponse = EntityUtils.toString(response.getEntity());
            return JsonResponse;

        } catch (ClientProtocolException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        } catch (IOException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
    }

    public int ExecuteProcess(String ProcessProperties, String ProcessID, String AtomID) throws IOException, ParseException {

        CredentialsProvider provider = BoomiUtils.GetCredsProvider(this.bSession);
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

        // URL
        String CompleteURL = BOOMI_BAS_URL + "/"+this.bSession.getAccountID().getInsecureString()+"/executeProcess";

        String payload = "{" + ProcessProperties +
                ",\"processId\": \""+ProcessID+"\", " +
                "\"atomId\": \""+AtomID+"\"" +
                "}";

        //System.out.println("DEBUG: "+payload);
        //System.out.println("DEBUG: "+CompleteURL);
        StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);

        HttpPost httpPost = new HttpPost(CompleteURL);
        httpPost.setEntity(entity);
        try{
            CloseableHttpResponse response = client.execute(httpPost);

            EntityUtils.toString(response.getEntity());

            int APIRetCode = response.getStatusLine().getStatusCode();
            return APIRetCode;

        }catch (Exception e){
            throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
        }


    }

}
