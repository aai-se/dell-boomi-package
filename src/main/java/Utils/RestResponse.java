package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.SelectItem;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

public class RestResponse{

    public static Table ProcessExecutionRecordListResponseAsCsv(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        Table table = new Table();

        List<Row> AllRows = new ArrayList<Row>();

        List<Schema> ListOfHeaders = new ArrayList<Schema>();
        ListOfHeaders.add(new Schema("type"));
        ListOfHeaders.add(new Schema("executionId"));
        ListOfHeaders.add(new Schema("account"));
        ListOfHeaders.add(new Schema("executionTime"));
        ListOfHeaders.add(new Schema("status"));
        ListOfHeaders.add(new Schema("executionType"));
        ListOfHeaders.add(new Schema("processName"));
        ListOfHeaders.add(new Schema("processId"));
        ListOfHeaders.add(new Schema("atomName"));
        ListOfHeaders.add(new Schema("atomId"));

        table.setSchema(ListOfHeaders);


        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){

            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjType = (String) ProcessInfo.get("@type");
            String ObjExcId = (String) ProcessInfo.get("executionId");
            String ObjAccount = (String) ProcessInfo.get("account");
            String ObjExecTime = (String) ProcessInfo.get("executionTime");
            String ObjStatus = (String) ProcessInfo.get("status");
            String ObjExecType = (String) ProcessInfo.get("executionType");
            String ObjProcName = (String) ProcessInfo.get("processName");
            String ObjProcId = (String) ProcessInfo.get("processId");
            String ObjAtomName = (String) ProcessInfo.get("atomName");
            String ObjAtomId = (String) ProcessInfo.get("atomId");

            Row aRow = new Row();
            List<com.automationanywhere.botcommand.data.Value> ListOfValues = new ArrayList<Value>();
            ListOfValues.add(new StringValue(ObjType));
            ListOfValues.add(new StringValue(ObjExcId));
            ListOfValues.add(new StringValue(ObjAccount));
            ListOfValues.add(new StringValue(ObjExecTime));
            ListOfValues.add(new StringValue(ObjStatus));
            ListOfValues.add(new StringValue(ObjExecType));
            ListOfValues.add(new StringValue(ObjProcName));
            ListOfValues.add(new StringValue(ObjProcId));
            ListOfValues.add(new StringValue(ObjAtomName));
            ListOfValues.add(new StringValue(ObjAtomId));

            aRow.setValues(ListOfValues);
            AllRows.add(aRow);
        }

        table.setRows(AllRows);
        return table;
    }

    public static String ProcessAtomListResponseAsString(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        String CSVResults = "";

        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){

            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjName = (String) ProcessInfo.get("name");
            String ObjId = (String) ProcessInfo.get("id");
            if(CSVResults.equalsIgnoreCase("")){
                CSVResults = ObjName+" ("+ObjId+")";
            }else{
                CSVResults = CSVResults+","+ObjName+" ("+ObjId+")";
            }

        }

        return CSVResults;
    }

    public static String ProcessProcessListResponseAsString(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        String CSVResults = "";

        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){

            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjName = (String) ProcessInfo.get("name");
            String ObjId = (String) ProcessInfo.get("id");
            if(CSVResults.equalsIgnoreCase("")){
                CSVResults = ObjName+" ("+ObjId+")";
            }else{
                CSVResults = CSVResults+","+ObjName+" ("+ObjId+")";
            }

        }

        return CSVResults;
    }

    public static List<SelectItem> ProcessAtomListResponseAsListOfItems(String JsonResponse) throws ParseException {

        List<SelectItem> SelectedItems = new ArrayList<SelectItem>();

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        String CSVResults = "";

        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){

            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjName = (String) ProcessInfo.get("name");
            String ObjId = (String) ProcessInfo.get("id");

            SelectItem anItem = new SelectItem(ObjName,ObjId);

            SelectedItems.add(anItem);

        }

        return SelectedItems;
    }

    public static List<SelectItem> ProcessProcessListResponseAsListOfItems(String JsonResponse) throws ParseException {

        List<SelectItem> SelectedItems = new ArrayList<SelectItem>();

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){


            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjName = (String) ProcessInfo.get("name");
            String ObjId = (String) ProcessInfo.get("id");

            SelectItem anItem = new SelectItem(ObjName,ObjId);

            SelectedItems.add(anItem);
        }

        return SelectedItems;
    }

    public static Table ProcessProcessListResponseAsCsv(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        Table table = new Table();

        List<Row> AllRows = new ArrayList<Row>();

        List<Schema> ListOfHeaders = new ArrayList<Schema>();
        ListOfHeaders.add(new Schema("type"));
        ListOfHeaders.add(new Schema("name"));
        ListOfHeaders.add(new Schema("id"));
        table.setSchema(ListOfHeaders);


        // Setting up rows
        JSONArray AllProcesses = (JSONArray) jobj.get("result");

        for(int i=0;i<AllProcesses.size();i++){

            JSONObject ProcessInfo = (JSONObject) AllProcesses.get(i);

            String ObjType = (String) ProcessInfo.get("@type");
            String ObjName = (String) ProcessInfo.get("name");
            String ObjId = (String) ProcessInfo.get("id");

            Row aRow = new Row();
            List<com.automationanywhere.botcommand.data.Value> ListOfValues = new ArrayList<Value>();
            ListOfValues.add(new StringValue(ObjType));
            ListOfValues.add(new StringValue(ObjName));
            ListOfValues.add(new StringValue(ObjId));

            aRow.setValues(ListOfValues);
            AllRows.add(aRow);
        }

        table.setRows(AllRows);
        return table;
    }

}