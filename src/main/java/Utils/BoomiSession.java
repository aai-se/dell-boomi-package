package Utils;

import com.automationanywhere.core.security.SecureString;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;

public class BoomiSession {
    public SecureString getSecureLogin() {
        return BoomiLogin;
    }
    public SecureString getSecurePwd() {
        return BoomiPassword;
    }
    public SecureString getAccountID() {
        return AccountID;
    }

    private SecureString BoomiLogin;
    private SecureString BoomiPassword;
    private SecureString AccountID;
    private Boolean PasswordIsApiToken;

    public BoomiSession(SecureString AccountID, SecureString Login, SecureString Pwd, Boolean IsPasswordAnAPIToken){
        this.BoomiLogin = Login;
        this.BoomiPassword = Pwd;
        this.AccountID = AccountID;
        this.PasswordIsApiToken = IsPasswordAnAPIToken;
    }


}
