package com.automationanywhere.botcommand.demo;

import Utils.RestResponse;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Convert Process List to CSV",
        name="ConvertProcessListToCSV",
        description="Convert Process List to CSV",
        icon="boomi.svg",
        node_label="Convert Process List to CSV",
        return_type= STRING,
        return_label="CSV of Processes",
        return_required=true,
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Converters"
)

public class ListProcessToCSV {

    private static final Logger logger = LogManager.getLogger(ListProcessToCSV.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "JSON Process List", default_value_type = STRING,  default_value = "") @NotEmpty String JSONInput
    )
    {

        String CSVRes = "";

        try {
            CSVRes = RestResponse.ProcessProcessListResponseAsString(JSONInput);
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
        StringValue sv = new StringValue();
        sv.set(CSVRes);
        return sv;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
