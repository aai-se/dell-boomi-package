package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.BoomiUtils;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Execute Process",
        name="ExecuteProcess",
        description="Trigger a Dell Boomi Process",
        icon="boomi.svg",
        node_label="Execute Process",
        return_type= DataType.NUMBER,
        return_label="Status",
        return_required=true,
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Processes"
)


public class ExecuteProcess {

    private static final Logger logger = LogManager.getLogger(ExecuteProcess.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public NumberValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.DESKTOPOPERATIONSELECT) @Pkg(label = "Process Name" , desktopOperationName = "DynamicProcessSelectLogic",desktopOperationAttributeNames = "sessionName") String ProcessID,
            @Idx(index = "3", type = AttributeType.DESKTOPOPERATIONSELECT) @Pkg(label = "Atom Name" , desktopOperationName = "DynamicAtomSelectLogic",desktopOperationAttributeNames = "sessionName") String AtomID,
            @Idx(index = "4", type = AttributeType.TEXTAREA) @Pkg(label = "Process Properties", default_value_type = STRING,  default_value = "\"ProcessProperties\" : {\"@type\" : \"ProcessProperties\",\"ProcessProperty\" : [{\"@type\" : \"\",\"Name\" : \"priority\",\"Value\": \"medium\"}]}") String ProcessProperties
    )
    {
        String ReformatedJson = "{"+ProcessProperties+"}";
        if(!BoomiUtils.isJSONValid(ReformatedJson)){throw new BotCommandException(MESSAGES.getString("FormatIssueProcessProp",ReformatedJson));}

        BoomiSession serv = (BoomiSession) this.sessions.get(sessionName);

        RestRequests BoomiRequests = new RestRequests(serv);
        int ApiRetCode = 0;
        try {
            ApiRetCode = BoomiRequests.ExecuteProcess(ProcessProperties, ProcessID, AtomID);
        } catch (IOException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }

        return new NumberValue(ApiRetCode);


    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
