package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.RestRequests;
import Utils.RestResponse;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="List Execution Records",
        name="ListExecutionRecords",
        description="List Execution Records",
        icon="boomi.svg",
        node_label="List Execution Records",
        return_type= DataType.TABLE,
        return_label="Table of Results",
        return_required=true,
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Execution Records"
)

public class ListExecutionRecords {

    private static final Logger logger = LogManager.getLogger(ListExecutionRecords.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public TableValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName
    )
    {

        BoomiSession serv = (BoomiSession) this.sessions.get(sessionName);

        RestRequests BoomiRequests = new RestRequests(serv);

        Table tableRes = new Table();
        TableValue TableRetVal = new TableValue();

        String JsonResponse = BoomiRequests.GetListOfExecutionRecords();
        //System.out.println("DEBUG:"+JsonResponse);
        try {
            tableRes = RestResponse.ProcessExecutionRecordListResponseAsCsv(JsonResponse);
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
        TableRetVal.set(tableRes);
        return TableRetVal;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
