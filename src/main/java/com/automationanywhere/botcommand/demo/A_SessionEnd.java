package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */

@BotCommand
@CommandPkg(
        label = "Session End",
        name = "SessionEnd",
        description = "Session End",
        icon = "boomi.svg",
        node_label = "session end {{sessionName}}",
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Admin"
)
public class A_SessionEnd {

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public void end(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,
                    default_value = "Default") @NotEmpty String sessionName) {

        BoomiSession serv  = (BoomiSession) this.sessions.get(sessionName);

        sessions.remove(sessionName);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}