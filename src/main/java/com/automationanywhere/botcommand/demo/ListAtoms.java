package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="List Atoms",
        name="ListAtoms",
        description="List Atoms",
        icon="boomi.svg",
        node_label="List Atoms",
        return_type= STRING,
        return_label="JSON Results",
        return_required=true,
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Atoms"
)


public class ListAtoms {

    private static final Logger logger = LogManager.getLogger(ListAtoms.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName

    )
    {

        BoomiSession serv = (BoomiSession) this.sessions.get(sessionName);

        RestRequests BoomiRequests = new RestRequests(serv);

        String JsonResponse = BoomiRequests.GetListOfAtoms();
        StringValue sv = new StringValue();
        sv.set(JsonResponse);
        return sv;
        //System.out.println("DEBUG:"+JsonResponse);
        /*
        try {
            tableRes = RestResponse.ProcessProcessListResponseAsCsv(JsonResponse);
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
        TableRetVal.set(tableRes);
        return TableRetVal;
*/
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
