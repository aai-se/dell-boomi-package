package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import Utils.RestRequests;
import Utils.RestResponse;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Convert Process List to Table",
        name="ConvertProcessListToTable",
        description="Convert Process List to Table",
        icon="boomi.svg",
        node_label="Convert Process List to Table",
        return_type= DataType.TABLE,
        return_label="Table of Results",
        return_required=true,
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Converters"
)

public class ListProcessToTable {

    private static final Logger logger = LogManager.getLogger(ListProcessToTable.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public TableValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "JSON Process List", default_value_type = STRING,  default_value = "") @NotEmpty String JSONInput
    )
    {

        Table tableRes = new Table();
        TableValue TableRetVal = new TableValue();

        try {
            tableRes = RestResponse.ProcessProcessListResponseAsCsv(JSONInput);
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }
        TableRetVal.set(tableRes);
        return TableRetVal;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
