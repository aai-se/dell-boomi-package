package com.automationanywhere.botcommand.demo;

import Utils.BoomiSession;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.core.security.SecureString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */


@BotCommand
@CommandPkg(
        label = "Session Start",
        name = "SessionStart",
        description = "Session Start",
        icon = "boomi.svg",
        node_label = "session start {{sessionName}}|",
        comment = true,
        text_color="#023d59",
        background_color = "#023d59",
        group_label="Admin"

)
public class A_SessionStart {

    private static final Logger logger = LogManager.getLogger(A_SessionStart.class);

    @Sessions
    private Map<String, Object> sessions;

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");


    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    private com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = AttributeType.CREDENTIAL) @Pkg(label = "Dell Boomi Accound ID",  default_value_type = STRING, default_value = "") @NotEmpty SecureString AccountID,
                      @Idx(index = "3", type = AttributeType.CREDENTIAL) @Pkg(label = "Dell Boomi Login",  default_value_type = STRING, default_value = "") @NotEmpty SecureString BoomiLogin,
                      @Idx(index = "4", type = AttributeType.CREDENTIAL) @Pkg(label = "Dell Boomi Password",  default_value_type = STRING, default_value = "") @NotEmpty SecureString BoomiPassword
    ) throws Exception {

        // Check for existing session
        if (this.sessions.containsKey(sessionName)){
            throw new BotCommandException(MESSAGES.getString("SessionNameInUse",sessionName)) ;
        }
        BoomiSession bms = new BoomiSession(AccountID,BoomiLogin,BoomiPassword,false);
        this.sessions.put(sessionName, bms);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
